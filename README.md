# Group_22

### General

This repository contains code to analyze the energy consumption and associated emissions per country from 1970 to 2021.

Students:
- Niclas Groll (48699@novasbe.pt)
- Julian Brenske (51283@novasbe.pt)
- Paul Florian Bork (50868@novasbe.pt)
- Erik Fubel (49155@novasbe.pt)


### Usage Instructions

In order to use the class for the analysis of energy data, make sure to set the working directory to this repo's main 
folder. After importing class EnergyAnalyzer from main_class.py in the 'src' directory, initialize the class with 
'EnergyAnalyzer()' and make sure to run the methods 'download()' and 'enrich()' to guarantee no complications in the 
further usage. To ease the setup of required packages, use the environment.yml file.

For information on features, please refer to the Sphinx documentation.

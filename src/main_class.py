"""
This module is dedicated to the analysis of energy data.

Classes:
    EnergyAnalyzer: A class to analyze energy data from 1970 to 2021. It contains various methods to visualize actual
    and forecasted consumption and emissions for given countries and years.
"""

import os
import warnings
from typing import Union
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from statsmodels.tsa.arima.model import ARIMA
warnings.filterwarnings("ignore")


class EnergyAnalyzer:
    """
    A tool to analyze the energy and consumption and mix of countries over the years from 1970 onwards.

    Attributes:
        df (pd.DataFrame): The energy data loaded into a pandas DataFrame.

    Methods:
        download()
        get_countries()
        plot_consumption(country, normalize)
        plot_compared_consumption(countries)
        plot_compared_gdp(countries)
        gapminder(year)
        plot_prediction(country, prediction_periods)
        scatter_emissions_consumption(year)
    """
    df: pd.DataFrame

    def __init__(self):
        self._enriched = False  # private attribute that is set to True once method enrich() has been executed

    def download(self) -> None:
        """
        Loads energy data from 1970 into attribute 'df'. If already downloaded, gets data from downloads/energy.csv.
        Else, downloads energy data into downloads directory.
        """
        # Define URL of dataset
        dataset_url = "https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv"

        # Define name of saved dataset file
        dataset_name = "energy.csv"

        # Get all filenames in downloads directory
        downloads_dir = os.getcwd() + "/downloads"
        downloads_dir_files = os.listdir(downloads_dir)

        # If there is no file with the name of our dataset, download it and save it to the downloads directory
        if dataset_name not in downloads_dir_files:
            df = pd.read_csv(dataset_url)
            df.to_csv(f"{downloads_dir}/{dataset_name}", index=False)

        # Else, read the saved file into a pandas DataFrame
        else:
            df = pd.read_csv(f"{downloads_dir}/{dataset_name}")

        # Filter for years after 1970 (inclusive)
        df = df[df['year'] >= 1970]

        # Covert 'date' to datetime object and set it as index
        df['year'] = pd.to_datetime(df['year'], format="%Y")
        df.set_index('year', inplace=True)

        # Set attribute 'df' to filtered DataFrame
        self.df = df

    def enrich(self) -> None:
        """
        Enriches the attribute 'df' with the column 'emissions' which is are the emissions in tonnes of CO2
        corresponding to the consumption of each energy type.
        """
        # Define emissions in gCO2 per kWh for each energy source
        source_emissions = {
            'biofuel': 1450,
            'coal': 1000,
            'gas': 455,
            'hydro': 90,
            'nuclear': 5.5,
            'oil': 1200,
            'solar': 53,
            'wind': 14
        }

        def get_emissions(df_row: pd.Series) -> float:
            """
            Calculates the emissions for a given row of attribute 'df'.

            Parameters:
                df_row (pd.Series): The row for which the emissions should be calculated.
            """
            # Create empty list in which emissions for each energy type are saved
            emissions = []

            for source_name, emission_multi in source_emissions.items():
                # Get the consumption value of row corresponding to the energy source by adding '_consumption' to source
                consumption = df_row[source_name + "_consumption"]

                # Calculate emissions in t CO2 by converting the consumption from kWh into TWh and the emissions from
                # grammes into tonnes
                source_emission = (consumption * 1e9) * (emission_multi / 1e6)

                # If the result of the calculation is not nan, append it to the emissions list
                if not np.isnan(source_emission):
                    emissions.append(source_emission)

            # Calculate and return the sum of emissions from all energy sources
            total_emissions = sum(emissions)
            return total_emissions

        # Get emissions column by calling function 'get_emissions' for each row of df
        self.df['emissions'] = self.df.apply(get_emissions, axis=1)

        # Set private attribute enriched to 'True' so that this method is not unnecessarily called multiple times
        self._enriched = True

    def get_countries(self) -> list:
        """
        Gets all countries present in the dataset.

        Returns:
            countries (list): List of all countries in dataset.
        """
        # Get all unique values of column 'countries' and transform the result to a list
        countries = self.df['country'].unique().tolist()
        return countries

    def plot_consumption(self, country: str, normalize: bool) -> None:
        """
        Creates a plot of the energy consumption per energy type (Oil, Gas, Renewables, etc.) of a given country.
        Depending on parameter 'normalize', the consumption is displayed in absolute (normalize=False) or relative
        (normalize=True) terms.

        Parameters:
            country (str): The country for which the consumption should be plotted.
            normalize (bool): Whether to plot relative (True) or absolute (False) consumption.

        Raises:
            ValueError: If the country passed in 'country' is not present in the dataset.
        """
        # Get all countries in dataset
        available_countries = self.get_countries()

        # If the passed country is not in dataset, raise error
        if country not in available_countries:
            raise ValueError(f"Country '{country}' not in dataset.")

        # Filter df for country
        country_df = self.df[self.df['country'] == country]

        # Get all columns names that contain values regarding consumption, i.e. that have suffix '_consumption'
        consumption_cols = [col for col in country_df.columns if '_consumption' in col]

        # Filter out those consumption column names that refer to aggregations of consumption of multiple energy sources
        aggr_consumption_cols = ['fossil_fuel_consumption', 'low_carbon_consumption', 'renewables_consumption',
                                 'primary_energy_consumption']
        relevant_consumption_cols = [c for c in consumption_cols if c not in aggr_consumption_cols]

        if normalize:
            # Create column 'total_consumption' by building the sum of all relevant consumption columns for each row
            country_df['total_consumption'] = country_df[relevant_consumption_cols].sum(axis=1)

            # For each relevant consumption column, get fraction of total consumption
            for col in relevant_consumption_cols:
                country_df[col] = country_df[col] / country_df['total_consumption']

        # Adjust the labels for each column name by removing suffix '_consumption', replacing underscores with spaces,
        # removing leading or trailing spaces, and converting the name to title cases
        labels = [e[: e.index("_consumption")].title().replace("_", " ").strip() for e in relevant_consumption_cols]

        # Plot the consumption and enhance graph
        plt.stackplot(country_df.index, country_df[relevant_consumption_cols].T, labels=labels)
        plt.title(f"Energy Consumption {country}")
        plt.xlabel('Year')
        plt.ylabel('Consumption in TWh' if not normalize else '% of Total Consumption')

        # Revert the order of labels in the legend to make it corresponding to the order of areas in the graph and place
        # the legend outside the graph
        handles, labels = plt.gca().get_legend_handles_labels()
        plt.legend(handles[::-1], labels[::-1], loc='upper left', bbox_to_anchor=(1, 1))
        plt.show()

    def plot_compared_consumption(self, countries: Union[str, list]) -> None:
        """
        Creates a plot that compares the total energy consumption and emissions for given countries.
        The total energy consumption is represented with a continuous line and bound to the left y-axis whilst the
        emissions are represented with a dashed line and bound to the right y-axis.

        Parameters:
            countries (str/list): The country or countries to plot the energy consumption for.

        Raises:
            ValueError: If one of the countries passed in 'countries' is not present in the dataset.
        """
        # If attribute 'df' hasn't yet been enriched with 'emissions' column, do so with method 'enrich'
        if not self._enriched:
            self.enrich()

        # If a string is passed for 'countries', put it in a list to allow for uniform subsequent operations
        if isinstance(countries, str):
            countries = [countries]

        # Get all columns names that contain values regarding consumption, i.e. that have suffix '_consumption'
        consumption_cols = [col for col in self.df.columns if '_consumption' in col]

        # Filter out those consumption column names that refer to aggregations of consumption of multiple energy sources
        aggr_consumption_cols = ['fossil_fuel_consumption', 'low_carbon_consumption', 'renewables_consumption',
                                 'primary_energy_consumption']
        relevant_consumption_cols = [c for c in consumption_cols if c not in aggr_consumption_cols]

        # Create plot with twinaxes
        ax = plt.subplots()[1]
        ax2 = ax.twinx()

        for country in countries:
            # If the country is not in dataset, raise error
            if country not in self.get_countries():
                raise ValueError(f"Country '{country}' not in dataset.")

            # Filter df for country
            country_df = self.df[self.df['country'] == country]

            # Create column 'total_consumption' by building the sum of all relevant consumption columns for each row
            country_df['total_consumption'] = country_df[relevant_consumption_cols].sum(axis=1)

            # Replace zeros with NaNs to avoid misleading lines dropping to zero in resulting graph
            country_df.replace({0: np.NaN}, inplace=True)

            # Plot consumption on left y-axis
            p = ax.plot(country_df.index, country_df['total_consumption'], label=country.title())

            # Get the color that was allocated to reuse for the plot on the right y-axis
            country_color = p[0].get_color()

            # Plot emissions on right y-axis
            ax2.plot(country_df.index, country_df['emissions'], '--', color=country_color, label=country.title())

        # Set title, labels and place legend of graph outside graph
        ax.set_title("Energy Consumption & Emissions per Country")
        ax.set_xlabel('Year')
        ax.set_ylabel('Consumption in TWh')
        ax2.set_ylabel('Emissions in Tonnes of C02')
        ax.legend(title="Consumption", loc='upper left', bbox_to_anchor=(1.1, 1))
        ax2.legend(title="Emissions", loc='lower left', bbox_to_anchor=(1.1, 0))
        plt.show()

    def plot_compared_gdp(self, countries: Union[str, list]) -> None:
        """
        Creates a plot that compares the GDP for given countries.

        Parameters:
            countries (str/list): The country or countries to plot the GDP for.

        Raises:
            ValueError: If one of the countries passed in 'countries' is not present in the dataset.
        """
        # If a string is passed for 'countries', put it in a list to allow for uniform subsequent operations
        if isinstance(countries, str):
            countries = [countries]

        # Get all unique years
        all_years = self.df.index.unique()

        # Create an empty DataFrame with the years as index. Each column will represent the GDP of a country in the year
        comparison_df = pd.DataFrame(index=all_years)

        for country in countries:
            # If the country is not in dataset, raise error
            if country not in self.get_countries():
                raise ValueError(f"Country '{country}' not in dataset.")

            # Filter df for country
            country_df = self.df[self.df['country'] == country]

            # Join the comparison df with the GDP of country on the index (year)
            comparison_df = comparison_df.join(country_df[['gdp']], how='left')

            # Rename the newly joined column according to the country
            comparison_df.rename({'gdp': country.title()}, axis=1, inplace=True)

        # Replace zeros with NaNs to represent missing data and make plot better interpretable
        comparison_df.replace({0: np.NaN}, inplace=True)

        # Plot data and adjust axis labels, legend, and title
        comparison_df.plot()
        plt.title("GDP per Country")
        plt.xlabel("Year")
        plt.ylabel("GDP (in USD, inflation-adjusted)")
        plt.legend()
        plt.show()

    def gapminder(self, year: int, log_scale_x: bool = False, log_scale_y: bool = False) -> None:
        """
        For a given year, creates a scatter plot where the x-axis is the GDP, the y-axis is the total energy consumption
        and the size of each dot is the population of the corresponding country.

        Parameters:
            year (int):  The year for which to create the plot.
            log_scale_x (bool, optional): Whether to plot the x-axis on a log-scale. Defaults to 'False'.
            log_scale_y (bool, optional): Whether to plot the y-axis on a log-scale. Defaults to 'False'.

        Raises:
            TypeError: If 'year' is not of type 'int'.
            Exception: If for 'year' not enough data is available.
        """
        # If the type of 'year' is not int, raise an error
        if not isinstance(year, int):
            raise TypeError("year is not of type int")

        # Filter df for specified year
        year_df = self.df[self.df.index.year == year]

        # Filter out of df values for world (iso_code == 'OWID_WRL') and for regions / continents (iso_code == NaN)
        year_df = year_df[year_df['iso_code'] != "OWID_WRL"].dropna(subset=['iso_code'])

        # Get all columns names that contain values regarding consumption, i.e. that have suffix '_consumption'
        consumption_cols = [col for col in self.df.columns if '_consumption' in col]

        # Filter out those consumption column names that refer to aggregations of consumption of multiple energy sources
        aggr_consumption_cols = ['fossil_fuel_consumption', 'low_carbon_consumption', 'renewables_consumption',
                                 'primary_energy_consumption']
        relevant_consumption_cols = [c for c in consumption_cols if c not in aggr_consumption_cols]

        # Create column 'total_consumption' by building the sum of all relevant consumption columns for each row
        year_df['total_consumption'] = year_df[relevant_consumption_cols].sum(axis=1)

        # If there is not at least one row that has non-nan values for 'gdp', 'total_consumption', & 'population', raise
        # error
        if len(year_df[['gdp', 'total_consumption', 'population']].dropna()) == 0:
            raise Exception(f"For year {year} there is not enough data available")

        # Plot data and adjust title, labels and legend
        sns.scatterplot(data=year_df, x='gdp', y='total_consumption', size='population')
        plt.title(f"GDP vs. Energy Consumption vs. Population ({year})")
        plt.xlabel(f"GDP (in USD, inflation-adjusted){' [log]' if log_scale_x else ''}")
        plt.ylabel(f"Energy Consumption in TWh{' [log]' if log_scale_y else ''}")
        plt.legend(title='Population\n(billion)')

        # Set scale of axes according to 'log_scale_x' and 'log_scale_y' - linear scale if 'False', log scale if 'True'
        plt.xscale('linear' if not log_scale_x else 'log')
        plt.yscale('linear' if not log_scale_y else 'log')

        plt.show()

    def plot_prediction(self, country: str, prediction_periods: int) -> None:
        """
        Plots the predicted energy consumption and emission for the given country and number of prediction periods.

        Parameters:
            country (str): The country to plot the predictions for.
            prediction_periods (int): The number of periods (years) to make predictions for.
        Raises:
            Exception: If 'prediction_periods' is not of type int.
            Exception: If 'prediction_periods' is smaller than 1.
        """
        # If attribute 'df' hasn't yet been enriched with 'emissions' column, do so with method 'enrich'
        if not self._enriched:
            self.enrich()

        # If the type of 'prediction_periods' is not int, raise an error
        if not isinstance(prediction_periods, int):
            raise Exception("prediction_periods is not of type int")

        # If the value of 'prediction_periods' is smaller than 1, raise an error
        if prediction_periods < 1:
            raise Exception("prediction_periods must be greater than or equal to 1")

        # Filter df for country
        country_df = self.df[self.df['country'] == country]

        # Get all columns names that contain values regarding consumption, i.e. that have suffix '_consumption'
        consumption_cols = [col for col in self.df.columns if '_consumption' in col]

        # Filter out those consumption column names that refer to aggregations of consumption of multiple energy sources
        aggr_consumption_cols = ['fossil_fuel_consumption', 'low_carbon_consumption', 'renewables_consumption',
                                 'primary_energy_consumption']
        relevant_consumption_cols = [c for c in consumption_cols if c not in aggr_consumption_cols]

        # Define the configuration for each prediction by defining the underlying data and the units of the prediction
        predictions_config = {
            # For emissions, use emission data but drop all rows with zeros or NaNs
            'emissions': {'data': country_df['emissions'].replace({0: np.NaN}).dropna(),
                          'units': 'Tonnes CO2'},
            # For consumption, use the sum of all relevant consumption columns but drop all rows with zeros or NaNs
            'consumption': {'data': country_df[relevant_consumption_cols].sum(axis=1).replace({0: np.NaN}).dropna(),
                            'units': 'TWh'}
        }

        # Create n horizontally aligned plots based on the number n of different predictions we want to make
        axes = plt.subplots(1, len(predictions_config), figsize=(12, 4))[1]

        for i, prediction in enumerate(predictions_config):
            # Get data for prediction
            data = predictions_config[prediction]['data']

            # Initiate ARIMA model
            model = ARIMA(data, order=(5, 1, 2))

            # Fit model
            model_fit = model.fit()

            # Make predictions for number of 'prediction_periods'
            predictions = model_fit.predict(len(data), len(data) + prediction_periods - 1)

            # Select ith graph to plot data and predictions on
            ax = axes[i]

            # Plot data and predictions and adjust titles and labels
            data.plot(ax=ax, color='blue', label='Actual')
            predictions.plot(ax=ax, color='orange', label='Prediction')
            ax.set_title(f"{prediction.title()} {country.title()}")
            ax.set_xlabel("Year")
            ax.set_ylabel(f"{prediction.title()} in {predictions_config[prediction]['units']}")
        plt.tight_layout()

        # Place a single legend below the most right graph
        plt.legend(loc='lower right', bbox_to_anchor=(1, -0.3), ncol=2)

    def scatter_emissions_consumption(self, year: int, log_scale_x: bool = False, log_scale_y: bool = False) -> None:
        """
        For a given year, creates a scatter plot where the x-axis is the emissions, the y-axis is the total energy
        consumption, and the size of each dot is the population of the corresponding country.

        Parameters:
            year (int): The year for which to create the plot.
            log_scale_x (bool, optional): Whether to plot the x-axis on a log-scale. Defaults to 'False'.
            log_scale_y (bool, optional): Whether to plot the y-axis on a log-scale. Defaults to 'False'.

        Raises:
            TypeError: If 'year' is not of type 'int'.
            Exception: If for 'year' not enough data is available.
        """
        # If 'year' is not if type int, raise an error
        if not isinstance(year, int):
            raise TypeError("year is not of type int")

        # If attribute 'df' hasn't yet been enriched with 'emissions' column, do so with method 'enrich'
        if not self._enriched:
            self.enrich()

        # Filter df for specified year
        year_df = self.df[self.df.index.year == year]

        # Filter out of df values for world (iso_code == 'OWID_WRL') and for regions / continents (iso_code == NaN)
        year_df = year_df[year_df['iso_code'] != "OWID_WRL"].dropna(subset=['iso_code'])

        # Get all columns names that contain values regarding consumption, i.e. that have suffix '_consumption'
        consumption_cols = [col for col in self.df.columns if '_consumption' in col]

        # Filter out those consumption column names that refer to aggregations of consumption of multiple energy sources
        aggr_consumption_cols = ['fossil_fuel_consumption', 'low_carbon_consumption', 'renewables_consumption',
                                 'primary_energy_consumption']
        relevant_consumption_cols = [c for c in consumption_cols if c not in aggr_consumption_cols]

        # Create column 'total_consumption' by building the sum of all relevant consumption columns for each row
        year_df['total_consumption'] = year_df[relevant_consumption_cols].sum(axis=1)

        # If there is not at least one row that has non-nan values for 'emissions', 'total_consumption', & 'population',
        # raise an error
        if len(year_df[['emissions', 'total_consumption', 'population']].dropna()) == 0:
            raise Exception(f"For year {year} there is not enough data available")

        # Plot data and adjust title, labels, and legend
        sns.scatterplot(data=year_df, x='emissions', y='total_consumption', size='population', color='green')
        plt.title(f"Emissions vs. Energy Consumption vs. Population ({year})")
        plt.xlabel(f"Emissions (in tonnes of CO2){' [log]' if log_scale_x else ''}")
        plt.ylabel(f"Energy Consumption in TWh{' [log]' if log_scale_y else ''}")
        plt.legend(title='Population\n(billion)')

        # Set scale of axes according to 'log_scale_x' and 'log_scale_y' - linear scale if 'False', log scale if 'True'
        plt.xscale('linear' if not log_scale_x else 'log')
        plt.yscale('linear' if not log_scale_y else 'log')

        plt.show()
